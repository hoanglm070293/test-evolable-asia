GRANT ALL ON `familytree`.* TO 'familytree'@'localhost' IDENTIFIED BY 'familytree@123';
DROP DATABASE IF EXISTS familytree;
CREATE DATABASE familytree;
GRANT ALL ON familytree.* TO familytree@localhost;

USE familytree;

CREATE TABLE IF NOT EXISTS `familytree`.`member` (
  `id`           INT(11)               AUTO_INCREMENT,
  `name`         NVARCHAR(45),
  `role`         NVARCHAR(45),
  `created_date` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `updated_date` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  AUTO_INCREMENT = 1 CHARACTER SET utf8 COLLATE utf8_unicode_ci;

INSERT INTO `familytree`.`member` (name, role) VALUES ('Oliver', 'grandfather'), ('Amelia', 'grandmother'), ('George', 'father'), ('Isla', 'mother'),
('Noah', 'brother'), ('Mia', 'sister');