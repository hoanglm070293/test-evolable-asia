import java.util.Arrays;

public class Answer2 {
    public static void main(String[] args) {
        Answer2 answer2 = new Answer2();
        System.out.println(answer2.findLongestPassword("qwerty-1qwerties-2 455 f0gfg 4 121kjn4qkdwkldjwakdjasd"));
    }

    public String findLongestPassword (String str) {
        String longestPassword = null;
        String replaceAllNumber = str.replaceAll("\\d", " ");
        String replaceAllBlank = replaceAllNumber.replaceAll(" ", "0");
        for (int i = 0; i < Arrays.asList(replaceAllBlank.trim().split("0")).size(); i++) {
            if (i > 0) {
                if (Arrays.asList(replaceAllBlank.trim().split("0")).get(i).length() > longestPassword.length()) {
                    longestPassword = Arrays.asList(replaceAllBlank.trim().split("0")).get(i);
                }
            } else {
                longestPassword = Arrays.asList(replaceAllBlank.trim().split("0")).get(0);
            }
        }
        return longestPassword;
    }
}

