public class Answer3 {
    public static void main(String[] args) {
        Answer3 answer4 = new Answer3();
        String input = "fafq212sa";
        System.out.println("Input: " + input);
        System.out.println("Encrypt: " + answer4.toEncrypt(input));
        System.out.println("Decrypt: " + answer4.toDecrypt(answer4.toEncrypt(input)));
    }

    public String toEncrypt(String text) {
        char alphabet[]={'A','B','C','D',
                'E','F','G','H',
                'I','J','K','L',
                'M','N','O','P',

                'Q','R','S','T',
                'U','V','W','X',
                'Y','Z', '0', '1',
                '2', '3', '4', '5',

                '6', '7', '8', '9'};

        String key[]={"RRR","RRG","RRB","RRY",
                "RGR","RGG","RGB","RGY",
                "RBR","RBG","RBB","RBY",
                "RYR","RYG","RYB","RYY",

                "GRR","GRG","GRB","GRY",
                "GGR","GGG","GGB","GGY",
                "GBR","GBG","GBB","GBY",
                "GYR","GYG","GYB","GYY",

                "BRR","BRG","BRB","BRY",
                "BGR","BGG","BGB","BGY",
                "BBR","BBG","BBB","BBY",
                "BYR","BYG","BYB","BYY",

                "YRR","YRG","YRB","YRY",
                "YGR","YGG","YGB","YGY",
                "YBR","YBG","YBB","YBY",
                "YYR","YYG","YYB","YYY",};
        String encrypted = "";
        for (int i = 0; i < text.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (alphabet[j] == text.toUpperCase().charAt(i)) {
                    encrypted = encrypted + key[j];
                }
            }
        }
        return encrypted;
    }

    public String toDecrypt(String encrypted) {
        char alphabet[]={'A','B','C','D',
                'E','F','G','H',
                'I','J','K','L',

                'M','N','O','P',
                'Q','R','S','T',
                'U','V','W','X',
                'Y','Z', '0', '1',

                '2', '3', '4', '5',
                '6', '7', '8', '9'};

        String key[]={"RRR","RRG","RRB","RRY",
                "RGR","RGG","RGB","RGY",
                "RBR","RBG","RBB","RBY",
                "RYR","RYG","RYB","RYY",

                "GRR","GRG","GRB","GRY",
                "GGR","GGG","GGB","GGY",
                "GBR","GBG","GBB","GBY",
                "GYR","GYG","GYB","GYY",

                "BRR","BRG","BRB","BRY",
                "BGR","BGG","BGB","BGY",
                "BBR","BBG","BBB","BBY",
                "BYR","BYG","BYB","BYY",

                "YRR","YRG","YRB","YRY",
                "YGR","YGG","YGB","YGY",
                "YBR","YBG","YBB","YBY",
                "YYR","YYG","YYB","YYY",};
        String decrypted = "";
        for (int i = 0; i < encrypted.length(); i +=  3) {
            String enkey = "";
            enkey = enkey + encrypted.charAt(i) + encrypted.charAt(i + 1) + encrypted.charAt(i + 2);
            for (int j = 0; j < key.length; j++) {
                if (key[j].equals(enkey)) {
                    decrypted = decrypted + alphabet[j];
                }
            }
        }
        return decrypted.toLowerCase();
    }
}

